import React, { Component } from 'react';
import { 
  SafeAreaView, 
  ScrollView,
  TouchableOpacity,
  StyleSheet, 
  Text, 
  View, 
  Alert, 
} from 'react-native';

export default class App extends Component {

  constructor(){
    super()
    this.state = {
      operands: '',
      operator: null,
      result: null
    }
    this.ops = ['DEL', '/', '%', '*', '-', '+']
  }

  buttonPressed = input => input === '=' ? this.calculate() : this.setState({operands: this.state.operands + input})

  clear = input => input === 'DEL' ? this.setState({operands: '', result: null}) : null

  operatorPressed = input => {
    if(input === 'DEL'){
      this.setState({operands: this.state.operands.slice(0, -1)})
      return
    }

    // allow '-' through and regect multiple entries of same operators
    if(this.ops.indexOf(this.state.operands.split('').pop()) > 0 && input !== '-' ) return
    
    this.setState({operands: this.state.operands + input})
  }

  calculate = () => {
    if(!this.state.operands) return 

    try {
      this.setState({result: eval(this.state.operands)})
    } catch (error) {
      Alert.alert('Syntax error', 'Invalid expression')
    }
  }
  
  render() {
    let rows = []
    let chars = [[7, 8, 9], [4, 5, 6], [1, 2, 3], ['.', 0, '=']]

    for(let i = 0; i < 4; i++){
      let row = []
      for(let j = 0; j < 3; j++){
        row.push(
          <TouchableOpacity onPress={() => this.buttonPressed(chars[i][j])} style={styles.number} key={j}>
            <Text style={styles.calText}>{chars[i][j]}</Text> 
          </TouchableOpacity>
        )
      }
      rows.push(<View style={styles.numbersRow} key={i}>{row}</View>)
    }

    let opsColumn = []

    for(let k = 0; k < this.ops.length; k++){
      opsColumn.push(
        <TouchableOpacity 
          onPress={() => this.operatorPressed(this.ops[k])} 
          onLongPress={() => this.clear(this.ops[k])} 
          style={styles.operation} key={k}
        >
          <Text style={[styles.calText, {color: 'white'}]}>{this.ops[k]}</Text>
        </TouchableOpacity>
      )
    }

    return (
      // <View style={{flex: 1}}>
      <SafeAreaView style={{flex: 1}}>

          <View style={styles.calculationContainer}>
            <ScrollView>
              <Text style={styles.text}>{this.state.operands}</Text>
              {/* <Text style={[styles.text, styles.operandsText]}>1234s</Text> */}
            </ScrollView>
          </View>  

          <View style={styles.resultContainer}>
            <ScrollView horizontal={true} contentContainerStyle={{flexDirection: 'row-reverse', alignItems: 'center'}}>
              <Text style={[styles.text, styles.resultText]}>{this.state.result}</Text>
              {/* <Text style={[styles.text, styles.resultText]}>12343</Text> */}
            </ScrollView>
          </View>          

        <View style={styles.buttons}>
          <View style={styles.numbersContainer}>
            {rows}
          </View>

          <View style={styles.operationsContainer}>
              {opsColumn}
          </View>
        </View>
      </SafeAreaView>  
    /* </View> */
    );
  }
}

const styles = StyleSheet.create({

  calculationContainer: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'flex-end',
    backgroundColor: '#f2f2f2',
  },

  resultContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-end',
    backgroundColor: '#e3e3e3',
  },

  numbersContainer: {
    flex: 4,
    backgroundColor: 'lightseagreen',
  },

  operationsContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'stretch',
    backgroundColor: '#212121',
    color: 'white'
  },

  text: {
    fontSize: 50,
    fontWeight: '500',
  },

  resultText: {
    color: '#666666', 
    alignSelf: 'center'
  },

  operandsText: {},

  buttons: {
    flex: 5,
    flexDirection: 'row',
    backgroundColor: 'green',
  },

  numbersRow: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center', 
    // borderWidth: 0.5,
  },

  number: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'stretch',
    // borderWidth: 0.5,
  },

  operation: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'stretch',
    // borderWidth: 0.5,
  },

  operationsText: {
    color: 'white',
    fontSize: 20,
  },

  calText: {
    fontSize: 35
  }
})